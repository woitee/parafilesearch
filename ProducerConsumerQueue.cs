﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ParallelSearch {
    /// <summary>
    /// A blocking queue.
    /// </summary>
    /// <typeparam name="T">Type of instances stored in the queue.</typeparam>
    public class ProducerConsumerQueue<T> {
        T[] array;
        int begin = 0; int end = 0; readonly int capacity; int used = 0;
        bool exiting = false;
        public ProducerConsumerQueue(int capacity) {
            array = new T[capacity];
            this.capacity = capacity;
        }

        public void Enqueue(T item) {
            lock (array) {
                if (exiting) return;
                while (used >= capacity) {
                    Monitor.Wait(array);
                    if (exiting) return;
                }
                array[end++] = item;
                end = end >= capacity ? 0 : end;
                ++used;
                //wake up blocked dequeue
                if (used == 1) {
                    Monitor.PulseAll(array);
                }
            }
        }

        public T Dequeue() {
            lock (array) {
                if (exiting) return default(T);
                while (used == 0) {
                    Monitor.Wait(array);
                    if (exiting) return default(T);
                }
                T ret = array[begin++];
                begin = begin >= capacity ? 0 : begin;
                --used;
                //wake up blocked enqueue
                if (used == capacity - 1) {
                    Monitor.PulseAll(array);
                }
                return ret;
            }
        }

        public void ReleaseAllBlocked() {
            lock (array) {
                exiting = true;
                Monitor.PulseAll(array);
            }
        }
    }
}
