﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ParallelSearch
{
    public partial class MainForm : Form {
        protected SortedDictionary<string, string> lowerToName
            = new SortedDictionary<string, string>();

        public MainForm() {
            InitializeComponent();
        }

        public void AddToListBox(string s) {
            statusLabel.BeginInvoke(
                (MethodInvoker)(() => {
                    filesListBox.Items.Add(s);
                })
            );
        }

        protected void refreshLabel() {
            statusLabel.BeginInvoke(
                (MethodInvoker) (() => {
                    statusLabel.Text = String.Format("{0} / {1} / {2} ERR / {3} MB", 
                                                     A, B, C, D.RoundDiv(1024*1024));
                })
            );
        }

        private int _a = 0, _b = 0, _c = 0; private long _d = 0; //label values
        public int A { set { _a = value; refreshLabel(); } get { return _a; } }
        public int B { set { _b = value; refreshLabel(); } get { return _b; } }
        public int C { set { _c = value; refreshLabel(); } get { return _c; } }
        public long D { set { _d = value; refreshLabel(); } get { return _d; } }
    }

    //divides two longs, rounded properly
    //doesnt work on longs using all 64 bits
    static class RoundDivExtension {
        public static long RoundDiv(this long divisor, long dividend) {
            long tmp = ((divisor * 2) / dividend);
            return tmp / 2 + tmp % 2;
        }
    }
}
