﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using Cuni.NPrg038;

namespace ParallelSearch {
    public static class EventExtensions {
        public static void Raise(this EventHandler evt, object sender, EventArgs e) {
            if (evt != null)
                evt(sender, e);
        }

        public static void Raise<T>(this EventHandler<T> evt, object sender, T e)
            where T : EventArgs {
            if (evt != null)
                evt(sender, e);
        }
    }

    class ParaSearch {
        CrawlerThread crawler;
        SearcherThread[] searchers;
        ProducerConsumerQueue<string> filenames;

        //events
        public class FilesFoundEventArgs : EventArgs {
            public FilesFoundEventArgs(int count) {
                this.Count = count;
            }
            public int Count { get; private set; }
        }
        public class PatternFoundEventArgs : EventArgs {
            public PatternFoundEventArgs(string filepath) {
                this.FilePath = filepath;
            }
            public string FilePath { get; private set; }
        }
        public class FileErrorEventArgs : EventArgs {
            public FileErrorEventArgs(int count) {
                this.Count = count;
            }
            public int Count { get; private set; }
        }
        public class DataReadEventArgs : EventArgs {
            public DataReadEventArgs(long bytes) {
                this.Bytes = bytes;
            }
            public long Bytes { get; private set; }
        }

        public event EventHandler<FilesFoundEventArgs> FilesFound;
        public event EventHandler<PatternFoundEventArgs> PatternFound;
        public event EventHandler<FileErrorEventArgs> FileError;
        public event EventHandler<DataReadEventArgs> DataRead;
        //end of events

        protected class CrawlerThread : ExtensibleThread {
            ProducerConsumerQueue<string> filenames;
            string startPath;
            Queue<string> unexploredDirs;
            public int errors = 0;
            ParaSearch parent;

            //management stuff
            //whether we are supposed to shutdown
            volatile bool shutdown = false;
            public CrawlerThread(ParaSearch parent, string startPath,
                                 ProducerConsumerQueue<string> filenames,
                                 Queue<string> unexploredDirs) {
                if (!File.Exists(startPath) && !Directory.Exists(startPath)) {
                    throw new FileNotFoundException();
                }
                this.parent = parent;
                this.filenames = filenames;
                this.startPath = startPath;
                this.unexploredDirs = unexploredDirs;
            }

            private void ExploreDirectory(string path) {
                try {
                    string[] files = Directory.GetFiles(path);
                    parent.FilesFound.Raise(parent, new FilesFoundEventArgs(files.Length));
                    foreach (string file in files) {
                        filenames.Enqueue(file);
                    }
                    foreach (string dir in Directory.GetDirectories(path)) {
                        unexploredDirs.Enqueue(dir);
                    }
                //Exceptions shouldn't be reacted to, just moving on with the search
                } catch (IOException) {
                    ++errors;
                    parent.FileError.Raise(parent, new FileErrorEventArgs(1));
                } catch (UnauthorizedAccessException) {
                    ++errors;
                    parent.FileError.Raise(parent, new FileErrorEventArgs(1));
                }
            }

            public override void Run() {
                if (File.Exists(startPath)) {
                    filenames.Enqueue(startPath);
                    return;
                } else if (Directory.Exists(startPath)) {
                    unexploredDirs.Enqueue(startPath);
                    while (unexploredDirs.Count > 0 && !shutdown) {
                        ExploreDirectory(unexploredDirs.Dequeue());
                    }
                }
            }

            public void Stop() {
                shutdown = true;
            }
        }

        protected class SearcherThread : ExtensibleThread {
            ProducerConsumerQueue<string> filenames;
            IByteSearch byteSearch;
            public int errors = 0;
            ParaSearch parent;

            //management stuff
            volatile bool shutdown = false;
            public SearcherThread(ParaSearch parent, ProducerConsumerQueue<string> filenames,
                                  IByteSearch byteSearch) {
                this.parent = parent;
                this.filenames = filenames;
                this.byteSearch = byteSearch;
            }

            private bool Search(string filename) {
                IByteSearchState state = byteSearch.InitialState;
                if (state.HasMatchedPattern) { return true; }
                FileStream reader = new FileStream(filename, FileMode.Open);
                try {
                    const int BUF_CAP = 16384;
                    byte[] buf = new byte[BUF_CAP];
                    int bytes = 0;
                    do {
                        bytes = reader.Read(buf, 0, BUF_CAP);
                        parent.DataRead.Raise(parent, new DataReadEventArgs(bytes));
 
                        for (int i = 0; i < bytes; ++i) {
                            state = state.GetNextState(buf[i]);
                            if (state.HasMatchedPattern) { return true; }
                        }
                    } while (bytes == BUF_CAP);
                } catch (IOException) {
                    parent.FileError.Raise(parent, new FileErrorEventArgs(1));
                } catch (System.Security.SecurityException) {
                    parent.FileError.Raise(parent, new FileErrorEventArgs(1));
                } catch (UnauthorizedAccessException) {
                    parent.FileError.Raise(parent, new FileErrorEventArgs(1));
                } finally {
                    reader.Close();
                }
                return false;
            }

            public override void Run() {
                //returns null after we are shutdown
                string filename = filenames.Dequeue();
                while (!shutdown) {
                    if (Search(filename)) {
                        parent.PatternFound.Raise(parent, new PatternFoundEventArgs(filename));
                    }
                    filename = filenames.Dequeue();
                }
            }

            public void Stop() {
                shutdown = true;
            }
        }

        public ParaSearch(int crawlThreads, int searchThreads, int queueLength) {
            //assume crawlThreads == 1
            filenames = new ProducerConsumerQueue<string>(queueLength);
            searchers = new SearcherThread[searchThreads];
        }

        /// <summary>
        /// Starts a parallel search, looking in all the files in given directory for a pattern.
        /// May throw an exception, don't call Stop or StopAndJoin if that happens, no Threads are started.
        /// </summary>
        /// <param name="startPath">Directory to search from.</param>
        /// <param name="pattern">Pattern to search in files.</param>
        /// <param name="matchFoundCallback">Function to call with every (string) filepath found.</param>
        /// <exception cref="FileNotFoundException"></exception>
        public void Start(string startPath, string pattern) {
            //if we'd to implement parallel crawling, this queue should be synchronized
            Queue<string> unexploredDirs = new Queue<string>();
            crawler = new CrawlerThread(this, startPath, filenames, unexploredDirs);
            crawler.Start();

            //create byteSearch for searcher threads to use
            IByteSearch byteSearch = new AhoCorasickSearch();
            byteSearch.AddPattern(Encoding.UTF8.GetBytes(pattern));    //UTF8
            byteSearch.AddPattern(Encoding.Unicode.GetBytes(pattern)); //UTF16 LittleEndian
            byteSearch.AddPattern(Encoding.Default.GetBytes(pattern)); //ANSI
            byteSearch.Freeze();

            //initialize searcher threads
            for (int i = 0; i < searchers.Length; ++i) {
                searchers[i] = new SearcherThread(this, filenames, byteSearch);
                searchers[i].Start();
            }
        }

        /// <summary>
        /// Notifies all threads that they should stop. Returns as soon as possible.
        /// </summary>
        public void Stop() {
            if (crawler != null)
                crawler.Stop();
            foreach (SearcherThread searcher in searchers) {
                if (searcher != null)
                    searcher.Stop();
            }
            filenames.ReleaseAllBlocked();
        }

        /// <summary>
        /// Notifies all threads that they should stop. Returns after all threads have stopped.
        /// </summary>
        public void StopAndJoin() {
            Stop();
            if (crawler != null)
                crawler.Join();
            foreach (SearcherThread searcher in searchers) {
                if (searcher != null && searcher != null)
                    searcher.Join();
            }
        }
    }
}
