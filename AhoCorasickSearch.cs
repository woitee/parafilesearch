﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cuni.NPrg038 {

	public interface IByteSearchState {
		IByteSearchState GetNextState(byte byteToProcess);
		IReadOnlyList<int> MatchedPatternIds { get; }
		bool HasMatchedPattern { get; }
	}

	public interface IFreezable {
		void Freeze();
		bool IsFrozen { get; }
	}

	public interface IByteSearch : IFreezable {
		void AddPattern(byte[] pattern);
		IByteSearchState InitialState { get; }
	}

	public class AhoCorasickSearch : IByteSearch {
	}

}