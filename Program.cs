﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ParallelSearchTests")]
namespace ParallelSearch {

    public static class Program {

        public static void MainBody(string[] args, TextReader input, TextWriter output) {
            //Parse command-line arguments
            string pattern, path; int n, m, x;
            try {
                if (args.Length != 5) {
                    throw new ArgumentException();
                }
                pattern = args[0];
                path = args[1];
                if (!File.Exists(path) && !Directory.Exists(path)) {
                    throw new ArgumentException();
                }
                n = int.Parse(args[2]);
                m = int.Parse(args[3]);
                x = int.Parse(args[4]);
                if (x <= 0 || n <= 0 || m <= 0) { throw new ArgumentException(); }
            } catch (ArgumentException) {
                output.WriteLine("Argument Error");
                return;
            } catch (FormatException) {
                output.WriteLine("Argument Error");
                return;
            }
            //Initialize form
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm form = new MainForm();

            //Start parallel search, all these methods should return ASAP
            var paraSearch = new ParaSearch(n, m, x);
            paraSearch.PatternFound += (sender, evt) => {
                form.AddToListBox(evt.FilePath);
                form.A += 1;
            };
            paraSearch.FilesFound += (sender, evt) => {
                form.B += evt.Count;
            };
            paraSearch.FileError += (sender, evt) => {
                form.C += evt.Count;
            };
            paraSearch.DataRead += (sender, evt) => {
                form.D += evt.Bytes;
            };

            //Create Form in this thread
            try {
                //paraSearch.Start returns quickly, but must be called after the form is shown
                //due to firing events that call methods of the form
                form.Shown += (sender, evt) => { paraSearch.Start(path, pattern); };
                //create a listener for closing event, we need to stop before the form is closed
                //due to firing events that call methods of the form
                form.FormClosing += (sender, evt) => { paraSearch.StopAndJoin(); };
                Application.Run(form);
            } finally {
                //stop all threads, in the case of form closed, this happens twice
                //but it is really fast
                paraSearch.StopAndJoin();
            }
        }

        [STAThread]
        static void Main(string[] args) {
            MainBody(args, Console.In, Console.Out);
        }
    }
}
