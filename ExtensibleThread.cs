﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ParallelSearch {
    //A Java-like interface for threading.
    //Not that i dig Java that much, but having Thread data, and the Thread together
    //is really useful in this situation.
    abstract class ExtensibleThread {
        protected Thread thread;

        public ExtensibleThread() {
            thread = new Thread(() => { Run(); });
        }

        public ThreadState ThreadState {
            get { return thread.ThreadState; }
        }

        public void Start() {
            thread.Start();
        }

        public abstract void Run();

        public void Join() {
            thread.Join();
        }
    }
}
